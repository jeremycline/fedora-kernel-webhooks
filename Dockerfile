FROM ubi8:latest

LABEL \
    name="webhooks" \
    license="MIT"

ENV USER=openshift
ADD . /app/fedora_kernel_webhooks
RUN yum install -y \
  python3 \
  python3-pip && \
  yum clean all; \
  python3 -m venv /opt/webhooks; \
  source /opt/webhooks/bin/activate; \
  pip install /app/fedora_kernel_webhooks; \
  pip install gunicorn
EXPOSE 8080
ENTRYPOINT ["/opt/webhooks/bin/gunicorn"]
CMD ["--bind=0.0.0.0:8080", "--config=/etc/fedora_kernel_webhooks/gunicorn.conf.py", "fedora_kernel_webhooks.wsgi"]
