# SPDX-License-Identifier: GPL-2.0-or-later
"""
Web Hook handlers.

To add a handler for a new event type, add a new function to the module and
register it to the :data:`WEBHOOKS` dictionary.
"""
import json
import logging
import os
from multiprocessing import dummy as mp_dummy

from django import http
from django.conf import settings
from django.views.decorators import csrf, http as http_decorators

import gitlab as gitlab_module


_log = logging.getLogger(__name__)

CONFIG_LABEL = "Configuration"

pool = None


@http_decorators.require_POST
@csrf.csrf_exempt
def web_hook(request: http.HttpRequest) -> http.HttpResponse:
    """
    Generic web hook handler for GitLab

    This is responsible for checking the authenticity of the request and
    dispatching it to the proper function based on the X-Gitlab-Event header.
    """
    try:
        secret = request.headers["X-Gitlab-Token"]
    except KeyError:
        return http.HttpResponseForbidden("Permission denied: missing web hook token")

    if secret != settings.FKW_GITLAB_WEBHOOK_SECRET:
        return http.HttpResponseForbidden("Permission denied: invalid web hook token")

    try:
        payload = json.loads(request.body, encoding=request.encoding)
    except json.JSONDecodeError:
        return http.HttpResponseBadRequest("JSON body required in POST")

    try:
        event_type = request.headers["X-Gitlab-Event"]
    except KeyError:
        return http.HttpResponseBadRequest('Missing "X-Gitlab-Event" header in request')

    try:
        handler = WEBHOOKS[event_type]
    except KeyError:
        _log.error(
            "No web hook handler for '%s' events; Adjust your web hooks on GitLab",
            request.headers["X-Gitlab-Event"],
        )
        return http.HttpResponseBadRequest("No web hook handler for request")

    _log.info("Handling web hook request with the '%s' handler", event_type)

    gitlab = gitlab_module.Gitlab(
        settings.FKW_GITLAB_URL, private_token=settings.FKW_GITLAB_TOKEN,
    )
    return handler(payload, gitlab)


def merge_request(payload: dict, gitlab: gitlab_module.Gitlab) -> http.HttpResponse:
    """
    Web hook for when a new merge request is created, an existing merge request
    was updated/merged/closed or a commit is added in the source branch

    The request body is a JSON document documented at
    https://docs.gitlab.com/ce/user/project/integrations/webhooks.html
    """
    project = gitlab.projects.get(payload["project"]["id"])
    merge_request = project.mergerequests.get(payload["object_attributes"]["iid"])
    global pool
    if pool is None:
        pool = mp_dummy.Pool()

    _apply_config_label(merge_request)
    pool.apply_async(
        _apply_subsystem_label, (merge_request.project_id, merge_request.iid)
    )
    return http.HttpResponse("Success!")


def _drop_ack_nack_labels(payload: dict, merge_request):
    """If the merge request got updated, clear any Acked-by/Nacked-by tags"""
    if payload["object_attributes"]["action"] != "update":
        _log.info("Skipping clearing acks/nacks since %r isn't updated", merge_request)
        return
    if "oldrev" not in payload["object_attributes"]:
        _log.info(
            "Merge request %r got updated, but no code changes were made so "
            "ack/naks are still valid",
            merge_request,
        )
        return

    merge_request.labels = [
        l for l in merge_request.labels if "Acked-by" not in l and "Nacked-by" not in l
    ]
    merge_request.save()


def _apply_subsystem_label(project_id: int, merge_request_id: int):
    """
    Apply a subsystem label to configuration merge requests. This is intended
    to run in a thread as it can take a long time to perform a code search and
    web hooks need to respond quickly. It's possible we'll miss labeling some
    merge requests if the process is killed after acking the web hook and before
    finishing this, but it's not a big deal.

    An entirely new GitLab client is created as the underlying requests session
    is not thread-safe.
    """
    gitlab = gitlab_module.Gitlab(
        settings.FKW_GITLAB_URL, private_token=settings.FKW_GITLAB_TOKEN,
    )
    project = gitlab.projects.get(project_id)
    merge_request = project.mergerequests.get(merge_request_id)
    subsystem_labels = set()
    for change in merge_request.changes()["changes"]:
        file_name = os.path.basename(change["new_path"])
        if file_name.startswith("CONFIG_"):
            results = project.search("blobs", f"config {file_name[7:]}")
            for path in [
                os.path.dirname(p["path"]) for p in results if "Kconfig" in p["path"]
            ]:
                # if drivers/net/ethernet/ do 4 parts at most
                # otherwise do 2 parts at most
                if path.startswith("drivers/net/ethernet/"):
                    subsystem_labels.update(
                        [f"Subsystem: {subsys}" for subsys in path.split("/")[:4]]
                    )
                else:
                    subsystem_labels.update(
                        [f"Subsystem: {subsys}" for subsys in path.split("/")[:2]]
                    )

    if subsystem_labels:
        merge_request.labels += list(subsystem_labels)
        merge_request.save()


def _apply_config_label(merge_request) -> None:
    """Add or remove the CONFIG_LABEL to merge requests."""
    config_dirs = [f"redhat/configs/{flavor}" for flavor in ("fedora", "common", "ark")]
    for change in merge_request.changes()["changes"]:
        for config_dir in config_dirs:
            if change["old_path"].startswith(config_dir) or change[
                "new_path"
            ].startswith(config_dir):
                merge_request.labels.append(CONFIG_LABEL)
                merge_request.save()
                return

    # Nothing touches the configuration so ensure the label is *not* applied.
    if CONFIG_LABEL in merge_request.labels:
        merge_request.labels = [
            label for label in merge_request.labels if label != CONFIG_LABEL
        ]
        merge_request.save()


# The set of web hook handlers that are currently supported. Consult `Gitlab
# webhooks`_ documentation for complete list of webhooks and the payload
# details.
#
# The key should be the value GitLab uses in the "X-Gitlab-Event" header of
# the web hook request.
#
# .. _Gitlab webhooks:
# https://docs.gitlab.com/ee/user/project/integrations/webhooks.html
WEBHOOKS = {
    "Merge Request Hook": merge_request,
}
